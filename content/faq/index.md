---
title: "FAQ"
---

### What is Prav?

Prav is an end-to-end encrypted by default, intuitive & easy-to-use free software instant messenger.

### Why the name 'Prav'?

'Prav' means pigeon in Malayalam, the emblem of universal peace.

### We already have many messenger apps available, what is the need for another one?

Most, if not all the instant messengers aren't [free software](https://www.gnu.org/philosophy/free-software-even-more-important.html)(that is, they do not respect users' freedom), which is a [major issue relating to the privacy & security of end-users](https://www.gnu.org/proprietary/proprietary.html). Prav, on the other hand, is free from such flaws, and is as simple to use as any other software. Prav is also decentralized and federates with other XMPP services. What's more, Prav is an Indian effort, specifically targeted for Indians. Chat with your dear ones, without having to worry about your data!

### I use every app for free? What are you talking about 'Free Software'?

When we speak of Free Software, we mean *"Free" as in "Freedom", not "Free of cost"*. The entire team of Prav, from the developers to the investors, including the web team and allies, are all ardent advocates of the free software philosophy.

### Is Prav a paid service, or free of cost?

Prav will be a paid service, but this demands some elaboration. Users can install Prav for no price from app stores but account creation will cost an amount. We don't intend to use your private conversations, nor your metadata to make money. If you want to use free of cost services, you will find many XMPP services that you can use without any fee, and you can still talk to Prav users. One example is Quicksy app for Android.

### How much will the account cost?

Currently, the mark has been set for ₹200 per user for a period of 3 years. That means users will have to renew their subscriptions every 3 years. However, this is a tentative decision and will depend upon how many users subscribe to our service.

### Wait! What will I do with Prav app if I don't want to pay for the account? I see this is a trick of yours to bind us into your service with your app.

Dear user, please note that this is not the case. As per our [**Social Contract**,](https://prav.app/social-contract//) our app is designed to allow you choose the XMPP service of your choice. We, the ones behind Prav are committed to make people know that instant messengers don't have to lock-in their users. Likewise, and conversely, Prav accounts can be used on other apps that support XMPP.

### What can I do with Prav? Will it offer me the facilities that other apps I use do?

Of course! Prav will do everything for you that you expect from an instant messenger. It has file sharing options, contact discovery, and of course, end-to-end encrypted by default messages!

### So, Prav runs on XMPP. What is this indeed? Give a summary, please.

[XMPP](https://en.wikipedia.org/wiki/XMPP) is an open protocol for exchange of information in real-time, and to be simple, it does so by the exchange of data in XML formats. As such, you can rest assured that all your data, and your metadata, is in the such a protocol which is open to verification, modification and extension. Added on top of that is another open encrytpion protocol [OMEMO](https://conversations.im/omemo/) you might have understood by now why Prav is a very secure and privacy-respecting messenger!
