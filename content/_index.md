---
title: 'Homepage'
meta_title: 'Prav App'
description: "Prav- privacy respecting chat app"
intro_image: ""
intro_image_absolute: false 
intro_image_hide_on_mobile: false
---

# Prav - Privacy Respecting Chatting App 

Prav is a social project aimed at providing Indian users a chatting app which will respect their [freedom](https://www.gnu.org/philosophy/free-software-even-more-important.html) and privacy, at the same time being interoperable, named 'Prav'. Prav app will be funded directly by users in form of subscription fee. To ensure that the decision-making process is democratic and transparent, we aim to register it as a [Multi State Cooperative Society](https://mscs.dac.gov.in/). 

We need 50 members from at least two Indian states to register as a cooperative, which marks our priority before launching the app. Diversity among our members is another top priority to increase representation from every community.

<style>
.button  
{
background-color: #ff1493;
}
</style
<div>
<a href="/become-a-member"><button class="button">Become A Member</button></a>
<br>
<br>
<p><b>Get in touch for any queries or feedback, Contact Email: prav@fsci.in</b></p>
<br>
<p style="font-size:36px"><b>Website Construction In Progress</b></p>

</div>

